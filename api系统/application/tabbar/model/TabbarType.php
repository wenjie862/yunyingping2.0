<?php
namespace app\tabbar\model;

use app\common\model\Base;

class TabbarType extends Base
{
    public function initialize()
    {
        parent::initialize();
        if (!isset($this->modelKey) || empty($this->modelKey) || cache('modelKey') != $this->modelKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>711]));
        }
    }

    public static function getFormatList()
    {
        $list = self::field('id,name')->select();
        $ret = [];
        foreach ($list as $key => $value) {
            $ret[$key]['value'] = $value['id'];
            $ret[$key]['label'] = $value['name'];
        }
        return $ret;
    }
}