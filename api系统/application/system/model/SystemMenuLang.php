<?php

namespace app\system\model;

use think\Model;

/**
 * 后台菜单语言模型
 * @package app\system\model
 */
class SystemMenuLang extends Model
{
    // 自动写入时间戳
    protected $autoWriteTimestamp = false;
    public function initialize()
    {
        parent::initialize();
        if (!isset($this->modelKey) || empty($this->modelKey) || cache('modelKey') != $this->modelKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>711]));
        }
    }
}
