<?php

namespace app\user\api;

use app\one_api\api\ApiInit;
use app\user\server\Auth as AuthServer;
use app\user\model\User;

class Auth extends ApiInit
{
    public function initialize()
    {
        parent::initialize();
        if (!isset($this->apiKey) || empty($this->apiKey) || cache('apiKey') != $this->apiKey) {
            return $this->_error('非法请求', [], 710);
        }
        $this->AuthServer = new AuthServer();
        $this->UserModel = new User();
    }

    /**统一入口 */
    public function dosth()
    {
        $data = $this->params;
        $result = $this->AuthServer->dosth($data);
        if (false === $result) {
            return $this->_error($this->AuthServer->getError(), '', $this->AuthServer->code ? $this->AuthServer->code : 1400);
        }
        return $this->_success("操作成功", $result);
    }

    public function logout()
    {
        $this->UserModel->logout($this->token);
        return $this->_success('退出成功');
    }

    public function certification()
    {
        $result = $this->UserModel->isLogin($this->token, true);
        if (false === $result) {
            return $this->_error($this->AuthServer->getError(), '', 1401);
        }
        return $this->_success("操作成功", $result);
    }
}
