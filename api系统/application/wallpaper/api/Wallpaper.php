<?php
namespace app\wallpaper\api;
use app\one_api\api\ApiInit;
use app\wallpaper\server\Wallpaper as WallpaperServer;

class Wallpaper extends ApiInit
{
    public function initialize() 
    {
        parent::initialize();
        if (!isset($this->apiKey) || empty($this->apiKey) || cache('apiKey') != $this->apiKey) {
            return $this->_error('非法请求', [], 710);
        }
        $this->WallpaperServer = new WallpaperServer();
    }
    
    public function getCategoryList()
    {
        $data= $this->params;
        $result = $this->WallpaperServer->getCategoryList($data);
        if (false === $result) {
            return $this->_error($this->WallpaperServer->getError(),'',90001);
        }
        return $this->_success("成功", $result);
    }
    
    public function getWallpaperList()
    {
        $data= $this->params;
        $result = $this->WallpaperServer->getWallpaperList($data);
        if (false === $result) {
            return $this->_error($this->WallpaperServer->getError(),'',90001);
        }
        return $this->_success("成功", $result);
    }
}