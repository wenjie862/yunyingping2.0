<?php
namespace app\videos\model;

use app\common\model\Base;

class Replay extends Base
{
    protected $name = "video_replay";

    public function initialize()
    {
        parent::initialize();
        if (!isset($this->modelKey) || empty($this->modelKey) || cache('modelKey') != $this->modelKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>711]));
        }
    }
}