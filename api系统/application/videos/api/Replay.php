<?php
namespace app\videos\api;
use app\one_api\api\UserInit;
use app\videos\server\Replay as ReplayServer;

class Replay extends UserInit
{

    public function initialize() 
    {
        // 是否验证登录
        $this->check_login = false;
        parent::initialize();
        if (!isset($this->apiKey) || empty($this->apiKey) || cache('apiKey') != $this->apiKey) {
            return $this->_error('非法请求', [], 710);
        }
        $this->ReplayServer = new ReplayServer();
    }

    /**
     * 获取评论列表
     *
     * @param [type] $data
     * @param [type] $user
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function getVodReplay()
    {
        $data= $this->params;
        $result = $this->ReplayServer->getVodReplay($data, $this->user);
        if (false === $result) {
            return $this->_error($this->ReplayServer->getError(),'',1002);
        }
        return $this->_success("成功", $result);
    }
    /**
     * 提交评论
     *
     * @param [type] $data
     * @param [type] $user
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function sendReplay()
    {
        $data= $this->params;
        $result = $this->ReplayServer->sendReplay($data, $this->user);
        if (false === $result) {
            return $this->_error($this->ReplayServer->getError(),'',1001);
        }
        return $this->_success("成功", $result);
    }

}