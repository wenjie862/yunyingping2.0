<?php
namespace app\codekey\api;
use app\one_api\api\UserInit;
use app\codekey\server\Codekey as CodekeyService;

class Codekey extends UserInit
{
    public function initialize() {
        parent::initialize();
        if (!isset($this->apiKey) || empty($this->apiKey) || cache('apiKey') != $this->apiKey) {
            return $this->_error('非法请求', [], 710);
        }
        $this->CodekeyService = new CodekeyService();
    }
    /**
     * 使用卡密
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function useCodekey() {
        $data = $this->params;
        $result = $this->CodekeyService->useCodekey($data, $this->user);
        if (false === $result) {
            return $this->_error($this->CodekeyService->getError(),'',9000);
        }
        return $this->_success('(*^◎^*)',$result);
    }

}