<?php

namespace app\news\api;
use app\one_api\api\ApiInit;
use app\news\server\News as NewsService;
use app\news\server\Msg as MsgService;

class Index extends ApiInit{
	protected function initialize()
    {
        parent::initialize();
        if (!isset($this->apiKey) || empty($this->apiKey) || cache('apiKey') != $this->apiKey) {
            return $this->_error('非法请求', [], 710);
        }
        $this->NewsService = new NewsService();
        $this->MsgService = new MsgService();
    }
    
    public function lists()
    {
        $data = $this->params;
        $result = $this->NewsService->getNewsLists($data);
        if (false === $result) {
            return $this->_error($this->BannerService->getError(),'',8000);
        }
        return $this->_success('获取成功',$result);
    }

    public function detail()
    {
        $data = $this->params;
        $result = $this->NewsService->getNewsDetail($data);
        if (false === $result) {
            return $this->_error($this->BannerService->getError(),'',8001);
        }
        return $this->_success('获取成功',$result);
    }

    public function msg()
    {
        $data = $this->params;
        $result = $this->MsgService->getMsgLists($data);
        if (false === $result) {
            return $this->_error($this->MsgService->getError(),'',8002);
        }
        return $this->_success('获取成功',$result);
    }
}