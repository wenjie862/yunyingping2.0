<?php

return [
    'db' => [
        // 数据库类型
        'type'        => 'mysql',
        // 服务器地址
        'hostname'    => '127.0.0.1',
        // 数据库名
        'database'    => '',
        // 数据库用户名
        'username'    => '',
        // 数据库密码
        'password'    => '',
        // 数据库编码默认采用utf8
        'charset'     => 'utf8',
        // 数据库表前缀
        'prefix'      => 'mac_',
        // 数据库调试模式
        'debug'       => false,
        // 如果数据库为外部服务器，打开下面注释，否则不需要
        // 'params'          => [
        //     \PDO::ATTR_PERSISTENT   => true,
        //     \PDO::ATTR_CASE         => \PDO::CASE_LOWER,
        // ],
        // // 是否需要断线重连
        // 'break_reconnect' => true,
    ]
];
