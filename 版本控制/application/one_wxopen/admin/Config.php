<?php
namespace app\one_wxopen\admin;
use app\system\admin\Admin;


class Config extends Admin
{
    public function index()
    {
        $file = env('app_path').'one_wxopen'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'wxopen.ini';
        if (file_exists($file) === true) {
            $formData = @parse_ini_file($file, true);
        } else {
            $formData = [];
        }
        if ($this->request->isAjax()) {
            $_data = input('post.');
            unset($_data['_csrf']);
            $_file = new \one\WriteIniFile($file);
            $_file->update($_data)->write();
            return $this->success('更新成功');
        }
        $formData['open_auth_domain'] = get_domain();
        $formData['open_ticket_url'] = get_domain().'/wxopen/ticket';
        $formData['open_msg_url'] = get_domain().'/wxopen/message/appid/$APPID$';
        $this->assign(compact('formData'));
        return $this->fetch();
    }
}