import Vue from 'vue';
import App from './App';
import requestApi from '@/common/vmeitime-http/http.js';
import configs from '@/common/config.js';
import functions from '@/utils/functions.js';
import {
    Platform,
    isAppPlus,
    isAppPlusNvue,
    isH5,
    isMpWeixin,
    isMpAlipay,
    isMpBaidu,
    isMpToutiao,
    isMpQQ,
    isMp360,
    isMp,
    isQuickappWebview,
    isQuickappWebviewUnion,
    isQuickappWebviewHuawei,
    isMpDouyinApp,
    isMpToutiaoApp
} from '@/utils/platform.js';
import {
    Base64
} from '@/utils/tools.js';

Vue.prototype.tools = new Base64();
//全局注入w-loading组件
import wLoading from "@/components/loading/w-loading.vue";
Vue.component('w-loading', wLoading)

import uView from 'uview-ui';
Vue.use(uView);

// 多语言开始
import zh from '@/common/locales/zh.js';
import en from '@/common/locales/en.js';
import VueI18n from 'vue-i18n';
Vue.use(VueI18n);
// 构造i18n对象
let language = uni.getStorageSync('language');
if (!language) {
    uni.setStorageSync('language', 'zh');
}
const i18n = new VueI18n({
    locale: language || 'zh',
    messages: {
        'zh': zh,
        'en': en
    }
})
// #ifdef MP-WEIXIN
Vue.prototype._i18n = i18n;
// #endif
// 多语言结束

Vue.prototype.$http = requestApi;
Vue.prototype.$config = configs;
Vue.prototype.$func = functions;

// 平台检测开始

Vue.prototype.$platform = Platform;
Vue.prototype.$isAppPlus = isAppPlus;
Vue.prototype.$isAppPlusNvue = isAppPlusNvue;
Vue.prototype.$isH5 = isH5;
Vue.prototype.$isMpWeixin = isMpWeixin;
Vue.prototype.$isMpAlipay = isMpAlipay;
Vue.prototype.$isMpBaidu = isMpBaidu;
Vue.prototype.$isMpToutiao = isMpToutiao;
Vue.prototype.$isMpQQ = isMpQQ;
Vue.prototype.$isMp360 = isMp360;
Vue.prototype.$isMp = isMp;
Vue.prototype.$isQuickappWebview = isQuickappWebview;
Vue.prototype.$isQuickappWebviewUnion = isQuickappWebviewUnion;
Vue.prototype.$isQuickappWebviewHuawei = isQuickappWebviewHuawei;
Vue.prototype.$isMpDouyinApp = isMpDouyinApp;
Vue.prototype.$isMpToutiaoApp = isMpToutiaoApp;

// 平台检测结束

// #ifdef APP-PLUS
if (uni.getSystemInfoSync().platform == "android") {
    Vue.prototype.$downloader = uni.requireNativePlugin('Karma617-DownloaderManager');
}
Vue.prototype.$sendTv = uni.requireNativePlugin("lyzml-DLNA");
// #endif


Vue.prototype.$os = uni.getSystemInfoSync().platform;
Vue.config.productionTip = false;

import mixin from '@/utils/base.js';
Vue.mixin(mixin);

App.mpType = 'app';

const app = new Vue({
    i18n,
    ...App
})
app.$mount();
