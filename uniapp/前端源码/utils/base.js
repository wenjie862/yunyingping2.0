import config from '../common/config.js';
import Vue from 'vue';
import App from '../App';
export default {
    data() {
        return {
            //设置默认的分享参数
            share: {
                title: '',
                path: '',
                imageUrl: '',
                desc: '',
                content: '',
                hard: 0
            },
            GlobalThemes: {
                commonBackGroundClass: 'bg-white',
                backGroundClass: 'bg-black',
                backGroundColor: '#282C35',
                backGroundStyle: 'background-color: #282C35; color: #FFFFFF',
                tarbarBgColor: "#282C35",
            },
            tabIndex: 0,
            shareTimeLineParams: '',
            froms: 0,
            sys_config: null
        }
    },
    onShareAppMessage(res) {
        let sys = uni.getStorageSync('sys_config');
        return {
            title: sys && sys.share_title && !this.share.hard ? sys.share_title : this.share.title,
            path: this.share.path,
            imageUrl: sys && sys.share_img && !this.share.hard ? config.apiImageUrl + sys.share_img : this.share.imageUrl,
            desc: this.share.desc,
            content: this.share.content,
            success(res) {
                uni.showToast({
                    title: '分享成功'
                })
            },
            fail(res) {
                uni.showToast({
                    title: '分享失败',
                    icon: 'none'
                })
            }
        }
    },
    onShareTimeline() {
        let sys = uni.getStorageSync('sys_config');
        return {
            title: this.share.title,
            query: this.shareTimeLineParams,
            imageUrl: sys && sys.share_img && !this.share.hard ? config.apiImageUrl + sys.share_img : this.share.imageUrl
        };
    },
    onLoad(options) {
        let routes = getCurrentPages();
        let curRoute = routes[routes.length - 1].route;
        let path = '/' + curRoute + (this.$u.queryParams(options).indexOf('?') != -1 ? this.$u.queryParams(options) +
            '&froms=1' : this.$u.queryParams(options) + '?froms=1');
        this.share.path = path;
        this.shareTimeLineParams = this.$u.queryParams(options, false) + '&froms=1'
    },
    mounted() {
        if (!uni.getStorageSync('tabIndex')) {
            uni.setStorageSync('tabIndex', 0);
        }
    },
    methods: {
        getCurrentPath() {
            let routes = getCurrentPages();
            let curRoute = routes[routes.length - 1].route;
            let curParam = routes[routes.length - 1].options;
            return '/' + curRoute + this.$u.queryParams(curParam);
        },
        GoBack() {
            this.froms = 0;
            let _this = this;
            let menuList = uni.getStorageSync('menuList').menu,
                path = '';
            let routes = getCurrentPages();
            let curRoute = routes[routes.length - 1].route;
            let curParam = routes[routes.length - 1].options;
            // 如果该页面是内页
            if (curParam.hasOwnProperty('froms')) {
                menuList.forEach((v, k) => {
                    // 寻找默认主页且默认主页非当前页面
                    if (v.isDefault) {
                        path = v.pagePath;
                        uni.setStorageSync('tabtarIndex', k);
                    }
                })
                if (('/' + curRoute) == path.split("?")[0]) {
                    // 否则默认跳到索引0的页面
                    path = menuList[0].pagePath;
                    uni.setStorageSync('tabtarIndex', 0);
                }
                uni.reLaunch({
                    url: path,
                    success() {
                        uni.removeStorageSync('tabtarIndex');
                        _this.getCommonSetting();
                    }
                })
            } else {
                uni.navigateBack();
            }
        },
        getCommonSetting() {
            let _this = this;
            // 全局配置
            Vue.prototype.$http
                .post('/videos/config/getConfig', {})
                .then(res => {
                    uni.setStorageSync('sys_config', res.data);
                    _this.sys_config = res.data;
                    App.globalData.adSwitchKg = res.data.ad_switch;
                })
                .then(v => {
                    _this.menuInit();
                })
                .then(v => {
                    _this.getAdlist();
                });
        },
        menuInit() {
            // 底部导航
            let language = uni.getStorageSync('language'),
                _this = this;
            language = language ? language : 'zh';
            Vue.prototype.$http.post('/tabbar/menu/getMenus', {}).then(res => {
                if (res.code == 200) {
                    App.globalData.tarbarObj.tarbarList = [];
                    res.data.list.forEach((v, k) => {
                        if (v.status) {
                            if (v.is_default == 1 && _this.sys_config.tarbar_up == 1) {
                                App.globalData.tarbarObj.midButton = true;
                                App.globalData.tarbarObj.tarbarList.push({
                                    iconPath: Vue.prototype.$config.apiImageUrl + v.icon,
                                    selectedIconPath: Vue.prototype.$config.apiImageUrl + v
                                        .active_icon,
                                    text: language != 'zh' ? v.nav_alias : v.nav_name,
                                    customIcon: false,
                                    midButton: true,
                                    pagePath: v.jump_url,
                                    isDefault: v.is_default
                                });
                            } else {
                                App.globalData.tarbarObj.tarbarList.push({
                                    iconPath: Vue.prototype.$config.apiImageUrl + v.icon,
                                    selectedIconPath: Vue.prototype.$config.apiImageUrl + v
                                        .active_icon,
                                    text: language != 'zh' ? v.nav_alias : v.nav_name,
                                    customIcon: false,
                                    midButton: false,
                                    pagePath: v.jump_url,
                                    isDefault: v.is_default
                                });
                            }
                        }
                    });
                    uni.setStorageSync('menuList', App.globalData.tarbarObj.tarbarList);
                } else {
                    uni.showModal({
                        title: App.globalData.$t('common').modelTitle,
                        content: res.msg || App.globalData.$t('common').failedNavigation,
                        showCancel: false
                    });
                }
            });
        },
        getAdlist() {
            let _this = this,
                tmpAdlist = [];
            Vue.prototype.$http.post('/videos/home/getAdList', {}).then(res => {
                let data = res.data;
                if (Vue.prototype.$isMpWeixin) {
                    tmpAdlist = data;
                } else {
                    let tmp = [];
                    data.forEach((v, k) => {
                        if (v.ad_type < 3) {
                            tmp.push(data[k]);
                        }
                    });
                    tmpAdlist = tmp;
                }
                uni.setStorageSync('adInfo', tmpAdlist);
            });
        },
    }
}
